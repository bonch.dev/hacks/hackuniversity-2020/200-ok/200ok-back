<?php

namespace App\Providers;

use App\Models\Art;
use App\Models\Drop;
use App\Models\Product\ProductType;
use App\Models\Product\ProductTypeAttribute;
use App\Observers\SlugObserver;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        \URL::forceScheme('https');

        Drop::observe([
            SlugObserver::class
        ]);

        Art::observe([
            SlugObserver::class
        ]);

        ProductType::observe([
            SlugObserver::class
        ]);

        ProductTypeAttribute::observe([
            SlugObserver::class
        ]);
    }
}
