<?php

namespace Tests\Feature\Artist;

use App\Models\Artist;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ProductsTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testSuccess()
    {
        $artist = Artist::first();

        $response = $this->getJson(
            route('artists.products', $artist)
        );

        $response->dump()
            ->assertStatus(200);
    }
}
