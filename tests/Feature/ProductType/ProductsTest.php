<?php

namespace Tests\Feature\ProductType;

use App\Models\Product\ProductType;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ProductsTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testSuccess()
    {
        $type = ProductType::first();

        $response = $this->getJson(
            route('catalog.types.products', $type)
        );

        $response->dump()
            ->assertStatus(200);
    }
}
