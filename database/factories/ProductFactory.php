<?php

/** @var Factory $factory */

use App\Models\Art;
use App\Models\Product;
use App\Models\Product\ProductType;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

$factory->define(Product::class, function (Faker $faker) {
    return [
        'art_id' => Art::inRandomOrder()->first()->id,
        'product_type_id' => ProductType::inRandomOrder()->first()->id
    ];
});
