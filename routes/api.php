<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('auth')->name('auth.')->group(function () {
    Route::post('/signin', 'AuthController@signin')->name('signin');
    Route::post('/signup', 'AuthController@signup')->name('signup');
    Route::get('/check', 'AuthController@check')->name('check');
});

Route::prefix('artists')
    ->name('artists.')
    ->group(function () {
        Route::get('/{artist}/products', 'ArtistController@products')->name('products');
        Route::post('/{artist}/commissions/start', 'CommissionController@start')->name('commissions.start');
        Route::apiResource('commissions', 'Artist\\CommissionController')->except([
            'store', 'destroy'
        ]);
    });

Route::prefix('catalog')->name('catalog.')->group(function () {
    Route::get('types/{type}/products', 'ProductTypeController@products')->name('types.products');
    Route::apiResource('types', 'ProductTypeController');
});

Route::prefix('drops')->name('drops.')->group(function () {
    Route::get('/{drop}/products', 'DropController@products')->name('products');
    Route::apiResource('/', 'DropController');
});

Route::apiResource('commissions', 'CommissionController')->except([
    'destroy', 'store'
]);

Route::apiResource('arts', 'ArtController');

//Route::apiResource('orders', 'OrderController');
