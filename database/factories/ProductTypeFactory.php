<?php

/** @var Factory $factory */

use App\Models\Product\ProductType;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;
use Illuminate\Support\Str;

$factory->define(ProductType::class, function (Faker $faker) {
    return [
        'slug' => Str::random(),
        'name' => Str::title($faker->unique()->words(random_int(1,4), true)),
        'price' => $faker->numberBetween(100000, 200000)
    ];
});
