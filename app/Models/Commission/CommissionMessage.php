<?php

namespace App\Models\Commission;

use App\Models\Commission;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class CommissionMessage extends Model
{
    protected $fillable = [
        'text'
    ];

    public function commission()
    {
        return $this->belongsTo(Commission::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
