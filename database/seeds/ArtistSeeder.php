<?php

use App\Models\Artist;
use App\Models\User;
use Illuminate\Database\Seeder;

class ArtistSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::all()->take(10)->each(function (User $user) {
            $user->artist()->save(
                factory(Artist::class)->make()
            );
        });
    }
}
