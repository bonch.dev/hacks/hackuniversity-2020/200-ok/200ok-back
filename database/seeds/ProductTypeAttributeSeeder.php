<?php

use App\Models\Product\ProductType;
use App\Models\Product\ProductTypeAttribute;
use Illuminate\Database\Seeder;

class ProductTypeAttributeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $attributes = json_decode(
            file_get_contents(database_path(
                'presets/product_type_attributes.json'
            )), true
        );

        foreach ($attributes as $attribute) {
            /** @var ProductTypeAttribute $productTypeAttribute */
            $productTypeAttribute = ProductTypeAttribute::create([
                'name' => $attribute['name'],
                'values' => $attribute['values'],
            ]);
            $productTypeAttribute->types()->sync($attribute['types']);
        }
    }
}
