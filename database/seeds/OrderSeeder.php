<?php

use App\Models\Order;
use App\Models\Order\OrderLine;
use Illuminate\Database\Seeder;

class OrderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Order::class, 20)->create()
            ->each(function (Order $order) {
                $order->lines()->saveMany(
                    factory(OrderLine::class, random_int(2,5))->make()
                );

                $order->update([
                    'total' => $order->lines()->sum('amount')
                ]);
            });
    }
}
