<?php

namespace Tests\Feature\Auth;

use App\Models\Artist;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Laravel\Sanctum\Sanctum;
use Tests\TestCase;

class SigninTest extends TestCase
{
    /**
     * @var User
     */
    private $user;

    protected function setUp(): void
    {
        parent::setUp();

        $this->user = factory(User::class)->create();
    }

    public function testUserSuccess()
    {
        $response = $this->post(
            route('auth.signin'),
            [
                'email' => $this->user->email,
                'password' => '123456'
            ]
        );

        $this->assertEquals(
            $this->user->tokens()->first()->abilities,
            ['acting-as-user']
        );

        $response->dump()
            ->assertStatus(200);
    }

    public function testArtistSuccess()
    {
        $this->user->artist()->save(
            factory(Artist::class)->make()
        );

        $response = $this->post(
            route('auth.signin'),
            [
                'email' => $this->user->email,
                'password' => '123456'
            ]
        );

        $this->assertEquals(
            $this->user->tokens()->first()->abilities,
            ['acting-as-user', 'acting-as-artist']
        );

        $response->dump()
            ->assertStatus(200);
    }
}
