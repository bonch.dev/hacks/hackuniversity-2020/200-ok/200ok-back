<?php

namespace Tests\Feature\Auth;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class SignupTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testSuccess()
    {
        $response = $this->postJson(
            route('auth.signup'),
            [
                'name' => 'Вася Пупкин',
                'email' => 'vasyan@mail.com',
                'password' => '12345678'
            ]
        );

        $response
            ->dump()
            ->assertStatus(200);
    }
}
