<?php

namespace Tests\Feature\Auth;

use App\Models\Artist;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Laravel\Sanctum\Sanctum;
use Tests\TestCase;

class CheckTest extends TestCase
{
    /**
     * @var User
     */
    private $user;

    protected function setUp(): void
    {
        parent::setUp();

        $this->user = factory(User::class)->create();
    }

    public function testUserSuccess()
    {
        Sanctum::actingAs(
            $this->user,
            ['acting-as-user']
        );

        $response = $this->getJson(
            route('auth.check')
        );

        $response->dump()
            ->assertStatus(200);
    }

    public function testArtistSuccess()
    {
        Sanctum::actingAs(
            $this->user,
            ['acting-as-user','acting-as-artist']
        );

        $this->user->artist()->save(
            factory(Artist::class)->make()
        );

        $response = $this->getJson(
            route('auth.check')
        );

        $response->dump()
            ->assertStatus(200);
    }
}
