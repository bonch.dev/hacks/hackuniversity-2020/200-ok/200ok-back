<?php

use App\Models\Product\ProductTypeAttribute;
use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = json_decode(
            file_get_contents(database_path(
                'presets/users.json'
            )), true
        );

        foreach ($users as $userData){
            /** @var User $user */
            $user = User::create([
                'name' => $userData['name'],
                'email' => $userData['email'],
                'password' => $userData['password'],
            ]);

            if ($userData['artist']) {
                $user->artist()->create(
                    $userData['artist']
                );
            }
        }
    }
}
