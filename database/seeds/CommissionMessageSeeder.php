<?php

use App\Models\Commission;
use Illuminate\Database\Seeder;

class CommissionMessageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Commission\CommissionMessage::class, 200)->create();
    }
}
