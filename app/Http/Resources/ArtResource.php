<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ArtResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'slug' => $this->slug,
            'name' => $this->name,
            'description' => $this->description,
            'drop' => new DropResource($this->whenLoaded('drop')),
            'artist' => $this->artist->user->only(['id', 'name']),
            'products' => ProductResource::collection(
                $this->whenLoaded('products')
            )
        ];
    }
}
