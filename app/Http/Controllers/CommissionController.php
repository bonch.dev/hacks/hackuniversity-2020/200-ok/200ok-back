<?php

namespace App\Http\Controllers;

use App\Http\Requests\Artist\Commission\CommissionArtistUpdateRequest;
use App\Http\Requests\Commission\CommissionStoreRequest;
use App\Http\Requests\Commission\CommissionUpdateRequest;
use App\Http\Resources\CommissionResource;
use App\Models\Artist;
use App\Models\Commission;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Http\Response;
use function response;

class CommissionController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth:sanctum', 'scope:acting-as-user'])->only([
            'start'
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return AnonymousResourceCollection
     */
    public function index()
    {
        $user = \request()->user();

        return CommissionResource::collection(
            $user->commissions()->paginate()
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function start(CommissionStoreRequest $request, Artist $artist)
    {
        $user = \request()->user();

        $commission = $user->commissions()->create(
            array_merge(
                $request->all(),
                [
                    'artist_id' => $artist->id
                ]
            )
        );

        return response()->json([
            'message' => 'Индивидуальный заказ отправлен художнику',
            'commission' => new CommissionResource(
                $commission
            )
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param Commission $commission
     * @return CommissionResource
     */
    public function show(Commission $commission)
    {
        return new CommissionResource(
            $commission
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Commission $commission
     * @return JsonResponse
     */
    public function update(CommissionUpdateRequest $request, Commission $commission)
    {
        if ($commission->status == 2) {
            return \response()->json([
                'message' => 'Вы больше не можете менять индивидуальное задание'
            ]);
        }

        $commission->update(
            $request->all()
        );

        if($commission->is_user_agree == $commission->is_artist_agree) {
            $commission->update([
                'status' => 2
            ]);
        }

        return \response()->json([
            'message' => 'Индивидуальный заказ был обновлен',
            'commission' => new CommissionResource($commission)
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Commission $commission
     * @return Response
     */
    public function destroy(Commission $commission)
    {
        //
    }
}
