<?php

use App\Models\Product\ProductType;
use Illuminate\Database\Seeder;

class ProductTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $types = json_decode(
            file_get_contents(database_path(
                'presets/product_types.json'
            )), true
        );

        foreach ($types as $type) {
            ProductType::create($type);
        }
    }
}
