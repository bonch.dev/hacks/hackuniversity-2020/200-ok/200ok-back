<?php

namespace App\Models;

use App\Models\Product\ProductType;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Product extends Model
{
    public function getPriceAttribute()
    {
        return $this->art->price + $this->type->price;
    }

    public function getFormatedPriceAttribute()
    {
        return ($this->art->price + $this->type->price) / 100;
    }

    public function art(): BelongsTo
    {
        return $this->belongsTo(Art::class);
    }

    public function type(): BelongsTo
    {
        return $this->belongsTo(
            ProductType::class,
            'product_type_id'
        );
    }
}
