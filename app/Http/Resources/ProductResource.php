<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Str;

class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'uuid' => Str::uuid(),
            'art' => new ArtResource($this->whenLoaded('art')),
            'type' => [
                'slug' => $this->type->slug,
                'name' => $this->type->name,
            ],
            'price' => $this->formated_price
        ];
    }
}
