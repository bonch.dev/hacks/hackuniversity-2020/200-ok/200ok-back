<?php

use App\Models\Art;
use App\Models\Drop;
use Illuminate\Database\Seeder;

class ArtSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $arts = json_decode(
            file_get_contents(database_path(
                'presets/arts.json'
            )), true
        );

        foreach ($arts as $artData) {
            Art::create($artData);
        }
    }
}
