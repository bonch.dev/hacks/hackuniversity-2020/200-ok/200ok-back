<?php

namespace Tests\Feature\Art;

use App\Models\Art;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ShowTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testSuccess()
    {
        $art = Art::first();

        $response = $this->getJson(
            route('arts.show', $art)
        );

        $response->dump()
            ->assertStatus(200);
    }
}
