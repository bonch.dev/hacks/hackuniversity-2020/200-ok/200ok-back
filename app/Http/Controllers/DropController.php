<?php

namespace App\Http\Controllers;

use App\Http\Resources\DropResource;
use App\Http\Resources\ProductResource;
use App\Models\Drop;
use Illuminate\Http\Request;

class DropController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        return DropResource::collection(
            Drop::paginate()
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Drop  $drop
     * @return DropResource
     */
    public function show(Drop $drop)
    {
        return new DropResource($drop);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Drop  $drop
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Drop $drop)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Drop  $drop
     * @return \Illuminate\Http\Response
     */
    public function destroy(Drop $drop)
    {
        //
    }

    /**
     * @param Drop $drop
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function products(Drop $drop)
    {
        return ProductResource::collection(
            $drop->products->load('art')
        );
    }
}
