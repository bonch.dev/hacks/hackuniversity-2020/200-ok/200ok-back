<?php

namespace App\Http\Controllers;

use App\Models\Product\ProductTypeAttribute;
use Illuminate\Http\Request;

class ProductTypeAttributeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Product\ProductTypeAttribute  $productTypeAttribute
     * @return \Illuminate\Http\Response
     */
    public function show(ProductTypeAttribute $productTypeAttribute)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Product\ProductTypeAttribute  $productTypeAttribute
     * @return \Illuminate\Http\Response
     */
    public function edit(ProductTypeAttribute $productTypeAttribute)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Product\ProductTypeAttribute  $productTypeAttribute
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProductTypeAttribute $productTypeAttribute)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Product\ProductTypeAttribute  $productTypeAttribute
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProductTypeAttribute $productTypeAttribute)
    {
        //
    }
}
