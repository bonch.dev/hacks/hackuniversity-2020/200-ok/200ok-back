<?php

namespace App\Models\Product;

use App\Models\Product;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class ProductType extends Model
{
    protected $fillable = [
        'slug', 'name', 'price'
    ];

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function products(): HasMany
    {
        return $this->hasMany(
            Product::class,
            'product_type_id'
        );
    }

    public function attributes()
    {
        return $this->belongsToMany(
            ProductTypeAttribute::class,
            'type_attribute',
            'type_id',
            'attribute_id'
        );
    }
}
