<?php

namespace App\Http\Controllers;

use App\Http\Requests\Auth\SigninRequest;
use App\Http\Requests\Auth\SignupRequest;
use App\Http\Resources\UserResource;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class AuthController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth:sanctum', 'scope:acting-as-user,acting-as-artist'])->only(['check']);
    }

    public function signin(SigninRequest $request)
    {
        /** @var User $user */
        $user = User::where('email', $request->email)->first();

        if(!$user || !Hash::check($request->password, $user->password)) {
            return \response()->json([
                'message' => 'Неверный пароль'
            ], 401);
        }

        $abilities = ['acting-as-user'];

        if($user->artist) {
            $abilities[] = 'acting-as-artist';
        }

        $token = $user->createToken(
            $user->id . '.' . Str::random(),
            $abilities
        );

        return \response()->json([
            'message' => 'Вы успешно вошли в систему',
            'access_token' => $token->plainTextToken
        ]);
    }

    public function signup(SignupRequest $request)
    {
        $user = User::create(
            $request->all()
        );

        return \response()->json([
            'message' => 'Регистрация прошла успешно',
            'user' => new UserResource($user)
        ]);
    }

    public function check()
    {
        /** @var User $user */
        $user = \request()->user();

        return \response()->json([
            'user' => new UserResource($user)
        ]);
    }
}
