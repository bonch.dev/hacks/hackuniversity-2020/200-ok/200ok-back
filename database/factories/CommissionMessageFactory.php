<?php

/** @var Factory $factory */

use App\Models\Commission;
use App\Models\Commission\CommissionMessage;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

$factory->define(CommissionMessage::class, function (Faker $faker) {
    $commission = Commission::inRandomOrder()->first();
    return [
        'commission_id' => $commission->id,
        'user_id' => $faker->randomElement([
            $commission->user_id,
            $commission->artist_id
        ]),
        'text' => $faker->text
    ];
});
