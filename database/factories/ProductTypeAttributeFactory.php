<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Product\ProductTypeAttribute;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(ProductTypeAttribute::class, function (Faker $faker) {
    return [
        'slug' => Str::random(),
        'name' => Str::title($faker->unique()->words(random_int(1,4), true)),
        'values' => $faker->words(random_int(1,10))
    ];
});
