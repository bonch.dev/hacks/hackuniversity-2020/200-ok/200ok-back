<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         $this->call([
             UserSeeder::class,
             DropSeeder::class,
             ArtSeeder::class,
             ProductTypeSeeder::class,
             ProductTypeAttributeSeeder::class,
             ProductSeeder::class,
//             OrderSeeder::class,
//             CommissionSeeder::class,
//             CommissionMessageSeeder::class
         ]);
    }
}
