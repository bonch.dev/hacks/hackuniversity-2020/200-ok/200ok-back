<?php

use App\Models\Product;
use App\Models\Product\ProductType;
use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $products = json_decode(
            file_get_contents(database_path(
                'presets/products.json'
            )), true
        );

        foreach ($products as $productData) {
            Product::create($productData);
        }
    }
}
