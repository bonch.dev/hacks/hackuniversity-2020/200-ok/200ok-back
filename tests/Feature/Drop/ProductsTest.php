<?php

namespace Tests\Feature\Drop;

use App\Models\Drop;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ProductsTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testSuccess()
    {
        $drop = Drop::first();

        $response = $this->getJson(
            route('drops.products', $drop)
        );

        $response->dump()
            ->assertStatus(200);
    }
}
