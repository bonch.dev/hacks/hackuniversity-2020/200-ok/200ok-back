<?php

namespace App\Models\Order;

use Illuminate\Database\Eloquent\Model;

class OrderLine extends Model
{
    protected $fillable = [
        'quantity',
        'price',
        'amount'
    ];

    protected $casts = [
        'attributes' => 'array'
    ];
}
