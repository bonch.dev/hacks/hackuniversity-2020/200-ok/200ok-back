<?php

namespace App\Observers;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class SlugObserver
{
    /**
     * Handle the model "creating" event.
     *
     * @param Model $model
     * @return void
     */
    public function creating(Model $model)
    {
        if (!$model->slug) {
            $model->slug = Str::slug($model->name);
        }
    }

    /**
     * Handle the model "updating" event.
     *
     * @param Model $model
     * @return void
     */
    public function updating(Model $model)
    {
        if (!$model->slug) {
            $model->slug = Str::slug($model->name);
        }
    }

    /**
     * Handle the model "deleted" event.
     *
     * @param Model $model
     * @return void
     */
    public function deleted(Model $model)
    {
        //
    }

    /**
     * Handle the model "restored" event.
     *
     * @param Model $model
     * @return void
     */
    public function restored(Model $model)
    {
        //
    }

    /**
     * Handle the model "force deleted" event.
     *
     * @param Model $model
     * @return void
     */
    public function forceDeleted(Model $model)
    {
        //
    }
}
