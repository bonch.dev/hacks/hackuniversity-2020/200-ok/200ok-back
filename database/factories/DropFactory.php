<?php

/** @var Factory $factory */

use App\Models\Artist;
use App\Models\Drop;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;
use Illuminate\Support\Str;

$factory->define(Drop::class, function (Faker $faker) {
    return [
        'artist_id' => Artist::inRandomOrder()->first()->id,
        'slug' => Str::random(),
        'name' => Str::title($faker->unique()->words(random_int(1, 4), true)),
        'description' => $faker->text
    ];
});
