<?php

/** @var Factory $factory */

use App\Models\Artist;
use App\Models\Commission;
use App\Models\Product\ProductType;
use App\Models\User;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

$factory->define(Commission::class, function (Faker $faker) {
    $user = User::inRandomOrder()->first()->id;
    $artist = Artist::where('id', '!=', $user)
        ->inRandomOrder()->first()->id;
    $type = ProductType::inRandomOrder()->first();

    $attributes = [];

    foreach ($type->attributes as $attribute) {
        foreach ($attribute->values as $value) {
            $attributes[$attribute->slug] = $value;
        }
    }

    $start_at = \Carbon\Carbon::now()->addDays(random_int(1,5));

    return [
        'user_id' => $user,
        'artist_id' => $artist,
        'product_type_id' => $type,
        'status' => $faker->randomElement([1,2,3,4]),
        'is_artist_agree' => $faker->boolean,
        'is_user_agree' => $faker->boolean,
        'requirements' => $faker->text,
        'attributes' => $attributes,
        'start_at' => $start_at,
        'end_at' => $start_at->addWeeks(random_int(1,3)),
        'price' => $faker->numberBetween(100000, 999900)
    ];
});
