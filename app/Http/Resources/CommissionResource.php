<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CommissionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'artist' => new ArtistResource($this->artist),
            'user' => new UserResource($this->user),
            'type' => new ProductTypeResource($this->type),
            'status' => $this->status,
            'is_artist_agree' => $this->is_artist_agree,
            'is_user_agree' => $this->is_user_agree,
            'requirements' => $this->requirements,
            'attributes' => $this->attributes,
            'start_at' => $this->start_at,
            'end_at' => $this->end_at,
            'price' => $this->formated_price
        ];
    }
}
