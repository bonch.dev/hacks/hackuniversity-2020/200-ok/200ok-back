<?php

namespace Tests\Feature\ProductType;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class IndexTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testSuccess()
    {
        $response = $this->getJson(
            route('catalog.types.index')
        );

        $response->dump()
            ->assertStatus(200);
    }
}
