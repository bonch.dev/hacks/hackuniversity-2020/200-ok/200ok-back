<?php

namespace App\Http\Requests\Artist\Commission;

use Illuminate\Foundation\Http\FormRequest;

class CommissionArtistUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'is_artist_agree' => 'boolean',
            'price' => 'numeric',
            'start_at' => 'date',
            'end_at' => 'date|nullable',
        ];
    }
}
