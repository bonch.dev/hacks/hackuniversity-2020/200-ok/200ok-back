<?php

namespace App\Http\Requests\Commission;

use Illuminate\Foundation\Http\FormRequest;

class CommissionStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'product_type_id' => 'exists:product_types,id',
            'requirements' => 'required|string',
            'price' => 'required|numeric',
            'start_at' => 'required|date',
            'end_at' => 'date|nullable'
        ];
    }
}
