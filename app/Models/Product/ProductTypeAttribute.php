<?php

namespace App\Models\Product;

use Illuminate\Database\Eloquent\Model;

class ProductTypeAttribute extends Model
{
    protected $fillable = [
        'name', 'slug', 'values'
    ];

    protected $casts = [
        'values' => 'array'
    ];

    public function types()
    {
        return $this->belongsToMany(
            ProductType::class,
            'type_attribute',
            'attribute_id',
            'type_id'
        );
    }
}
