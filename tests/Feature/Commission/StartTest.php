<?php

namespace Tests\Feature\Commission;

use App\Models\Artist;
use App\Models\Product\ProductType;
use App\Models\User;
use Carbon\Carbon;
use Faker\Factory;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Laravel\Sanctum\Sanctum;
use Tests\TestCase;

class StartTest extends TestCase
{
    /** @var User */
    private $user;
    /**
     * @var \Faker\Generator
     */
    private $faker;

    protected function setUp(): void
    {
        parent::setUp();

        $this->user = User::find(2);
        $this->faker = Factory::create();
    }

    public function testSuccess()
    {
        Sanctum::actingAs(
            $this->user,
            ['acting-as-user']
        );

        $artist = Artist::first();

        $data = [
            'product_type_id' => ProductType::first()->id,
            'requirements' => $this->faker->text,
            'price' => $this->faker->numberBetween(1000, 2000),
            'start_at' => Carbon::now()
        ];

        $response = $this->postJson(
            route('artists.commissions.start', $artist),
            $data
        );

        $response->dump()
            ->assertStatus(200);
    }
}
