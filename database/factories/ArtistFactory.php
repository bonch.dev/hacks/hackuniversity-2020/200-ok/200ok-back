<?php

/** @var Factory $factory */

use App\Models\Artist;
use App\Models\User;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

$factory->define(Artist::class, function (Faker $faker) {
    return [
        'is_active' => $faker->boolean
    ];
});
