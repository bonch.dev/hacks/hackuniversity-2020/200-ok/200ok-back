<?php

/** @var Factory $factory */

use App\Models\Order;
use App\Models\Order\OrderLine;
use App\Models\Product;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

$factory->define(OrderLine::class, function (Faker $faker) {
    $order = Order::inRandomOrder()->first();
    /** @var Product $product */
    $product = Product::inRandomOrder()->first();
    $quantity = $faker->numberBetween(1,10);

    $attributes = [];

    foreach ($product->type->attributes as $attribute) {
        foreach ($attribute->values as $value) {
            $attributes[$attribute->slug] = $value;
        }
    }

    $price = $product->price;
    $amount = $price * $quantity;

    return [
        'order_id' => $order->id,
        'product_id' => $product->id,
        'quantity' => $faker->numberBetween(1,10),
        'attributes' => $attributes,
        'price' => $price,
        'amount' => $amount
    ];
});
