<?php

use App\Models\Drop;
use App\Models\Product\ProductTypeAttribute;
use Illuminate\Database\Seeder;

class DropSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $drops = json_decode(
            file_get_contents(database_path(
                'presets/drops.json'
            )), true
        );

        foreach ($drops as $dropData) {
            Drop::create($dropData);
        }
    }
}
