<?php

namespace App\Http\Controllers\Artist;

use App\Http\Controllers\Controller;
use App\Http\Requests\Artist\Commission\CommissionArtistUpdateRequest;
use App\Http\Resources\CommissionResource;
use App\Models\Artist;
use App\Models\Commission;
use Illuminate\Http\Request;

class CommissionController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth:sanctum', 'scope:acting-as-artist'])->only([
            'start'
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        /** @var Artist $artist */
        $artist = \request()->user()->artist;

        return CommissionResource::collection(
            $artist->commissions()->paginate()
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Commission  $commission
     * @return CommissionResource
     */
    public function show(Commission $commission)
    {
        return new CommissionResource(
            $commission
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Commission  $commission
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(CommissionArtistUpdateRequest $request, Commission $commission)
    {
        if ($commission->status == 2) {
            return \response()->json([
                'message' => 'Вы больше не можете менять индивидуальное задание'
            ]);
        }

        $commission->update(
            $request->all()
        );

        if($commission->is_user_agree == $commission->is_artist_agree) {
            $commission->update([
                'status' => 2
            ]);
        }

        return \response()->json([
            'message' => 'Индивидуальный заказ был обновлен',
            'commission' => new CommissionResource($commission)
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Commission  $commission
     * @return \Illuminate\Http\Response
     */
    public function destroy(Commission $commission)
    {
        //
    }
}
