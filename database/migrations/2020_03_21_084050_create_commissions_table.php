<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCommissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('commissions', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('artist_id');
            $table->unsignedBigInteger('product_type_id');
            /**
             * Statuses (may be more)
             * 1 – discussion
             * 2 – accepted
             * 3 – declined
             * 4 – done
             */
            $table->enum('status', [
                1, 2, 3
            ])->default(1)->index();
            $table->boolean('is_artist_agree')->nullable();
            $table->boolean('is_user_agree')->nullable();
            $table->text('requirements');
            $table->date('start_at');
            $table->date('end_at')->nullable();
            $table->integer('price');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('commissions');
    }
}
