<?php

namespace App\Models;

use App\Models\Order\OrderLine;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = [
        'phone', 'address', 'status'
    ];

    protected $casts = [
        'address' => 'json'
    ];

    public function scopeCompleted(Builder $query)
    {
        $query->whereIn('status', [2]);
    }

    public function scopeProgressing(Builder $query)
    {
        $query->whereIn('status', [1]);
    }

    public function lines()
    {
        return $this->hasMany(OrderLine::class);
    }
}
