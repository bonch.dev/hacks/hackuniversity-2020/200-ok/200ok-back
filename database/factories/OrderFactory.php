<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Order;
use Faker\Generator as Faker;

$factory->define(Order::class, function (Faker $faker) {
    return [
        'user_id' => \App\Models\User::inRandomOrder()->first()->id,
        'status' => $faker->randomElement([1, 2]),
        'phone' => $faker->e164PhoneNumber,
        'address' => [
            'city' => $faker->city,
            'street' => $faker->streetName,
        ],
        'total' => $faker->numberBetween(600000, 1000000)
    ];
});
