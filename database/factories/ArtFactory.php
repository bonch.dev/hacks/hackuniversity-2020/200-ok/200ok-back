<?php

/** @var Factory $factory */

use App\Models\Art;
use App\Models\Artist;
use App\Models\Drop;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;
use Illuminate\Support\Str;

$factory->define(Art::class, function (Faker $faker) {
    return [
        'slug' => Str::random(),
        'name' => Str::title(
            $faker->unique()->words(random_int(1,3), true) .
            $faker->unique()->words(random_int(1,2), true)
        ),
        'description' => $faker->text,
        'price' => $faker->numberBetween(100000, 999900),
        'drop_id' => $faker->randomElement([
            Drop::inRandomOrder()->first()->id,
            null
        ]),
        'artist_id' => Artist::inRandomOrder()->first()->id
    ];
});
