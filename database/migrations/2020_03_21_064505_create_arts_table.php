<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateArtsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('arts', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('drop_id')->nullable();
            $table->unsignedBigInteger('artist_id');
            $table->string('slug')->unique();
            $table->string('name');
            $table->integer('price');
            $table->text('description');
            $table->timestamps();

            $table->foreign('drop_id')
                ->references('id')->on('drops');
            $table->foreign('artist_id')
                ->references('id')->on('artists');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('arts');
    }
}
