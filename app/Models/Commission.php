<?php

namespace App\Models;

use App\Models\Commission\CommissionMessage;
use App\Models\Product\ProductType;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Commission extends Model
{
    protected $fillable = [
        'user_id',
        'artist_id',
        'product_type_id',
        'status',
        'is_artist_agree',
        'is_user_agree',
        'requirements',
        'attributes',
        'start_at',
        'end_at',
        'price',
    ];

    protected $casts = [
        'attributes' => 'array'
    ];

    public function getFormatedPriceAttribute()
    {
        return $this->price / 100;
    }

    public function setPriceAttribute(float $price)
    {
        $this->attributes['price'] = $price * 100;
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function artist()
    {
        return $this->belongsTo(Artist::class);
    }

    public function type(): BelongsTo
    {
        return $this->belongsTo(
            ProductType::class,
            'product_type_id'
        );
    }

    public function messages()
    {
        return $this->hasMany(
            CommissionMessage::class,
            'commission_id'
        );
    }
}
